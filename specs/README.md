# MIL-DTL-55116 Specifications

Military specifications for 5-pin and 6-pin connectors commonly used for audio, data, key fill, and other purposes in US military tactical communications equipment.

Title                                                                                                | Specification                                  | Date
-----------------------------------------------------------------------------------------------------|------------------------------------------------|-----------
Connectors: Miniature Audio, Five-Pin and Six-Pin, General Specification                             | [MIL-DTL-55116D](dtl55116.pdf)                 | 2015-12-22
Connectors: Miniature Audio, Five-Pin and Six-Pin, Supplement                                        | [MIL-DTL-55116 SUPPLEMENT 1](dtl55116sup1.pdf) | 2004-03-22
Connector, Plug, Five Pin Audio, Crimp Sleeve Terminals, Wire Strain Relief, U-229 Type              | [MIL-DTL-55116/1B](dtl55116ss1.pdf)            | 2004-03-22
Connector, Plug, Six Pin Audio, Crimp Sleeve Terminals, Wire Strain Relief, U-229 Type               | [MIL-DTL-55116/2B](dtl55116ss2.pdf)            | 2004-03-22
Connector, Plug, Five Pin Audio, Solder Cup Terminals, Wire Strain Relief, U-229 Type                | [MIL-DTL-55116/3B](dtl55116ss3.pdf)            | 2004-03-22
Connector, Plug, Six Pin Audio, Solder Cup Terminals, Wire Strain Relief, U-229 Type                 | [MIL-DTL-55116/4B](dtl55116ss4.pdf)            | 2004-03-22
Connector, Plug, Five Pin Audio, Crimp Sleeve Terminals, Molded Strain Relief, U-182 Type            | [MIL-DTL-55116/5B](dtl55116ss5.pdf)            | 2004-03-22
Connector, Plug, Six Pin Audio, Crimp Sleeve Terminals, Molded Strain Relief, U-182 Type             | [MIL-DTL-55116/6B](dtl55116ss6.pdf)            | 2004-03-22
Connector, Plug, Five Pin Audio, Solder Cup Terminals, Molded Strain Relief, U-182 Type              | [MIL-DTL-55116/7B](dtl55116ss7.pdf)            | 2004-03-22
Connector, Plug, Six Pin Audio, Solder Cup Terminals, Molded Strain Relief, U-182 Type               | [MIL-DTL-55116/8B](dtl55116ss8.pdf)            | 2004-03-22
Connector, Receptacle, Five Pin Audio, Solder Cup Spring Terminals, Panel Mount, U-183 Type          | [MIL-DTL-55116/9C](dtl55116ss9.pdf)            | 2013-08-01
Connector, Receptacle, Six Pin Audio, Solder Cup Spring Terminals, Panel Mount, U-183 Type           | [MIL-DTL-55116/10B](dtl55116ss10.pdf)          | 2000-12-11
Connector, Receptacle, Five Pin Audio, Crimp Sleeve Spring Terminals, Wire Strain Relief, U-228 Type | [MIL-DTL-55116/11B](dtl55116ss11.pdf)          | 2004-03-22
Connector, Receptacle, Six Pin Audio, Crimp Sleeve Spring Terminals, Wire Strain Relief, U-228 Type  | [MIL-DTL-55116/12B](dtl55116ss12.pdf)          | 2004-03-22
Connector, Receptacle, Five Pin Audio, Solder Cup Spring Terminals, Wire Strain Relief, U-228 Type   | [MIL-DTL-55116/13B](dtl55116ss13.pdf)          | 2004-03-22
Connector, Receptacle, Six Pin Audio, Solder Cup Spring Terminals, Wire Strain Relief, U-228 Type    | [MIL-DTL-55116/14B](dtl55116ss14.pdf)          | 2004-03-22
