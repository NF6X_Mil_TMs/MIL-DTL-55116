# MIL-DTL-55116 Connectors

This is my personal cache of documentation related to MIL-DTL-55116 series 5-pin and 6-pin connectors commonly used for audio, data, key fill, and other purposes in US military tactical communications equipment.

The documents are sorted into these subdirectories:

Directory                  | Description
---------------------------|---------------------------------------
[specs](specs/README.md)   | US military specifications
[vendor](vendor/README.md) | Vendor catalogs and datasheets
