# Vendor Catalogs and Datasheets

These are vendors catalogs and datasheets including MIL-DTL-55116 compatible connectors. Some of them also include other related connector families.

Description                                                            | File
-----------------------------------------------------------------------|-------------------------------------------------------------------
Amphenol 164 series                                                    | [164series-171.pdf](164series-171.pdf)
Eaton C4ISR/tactical communications cables and connectors              | [C4ISR_taccom_Eaton.pdf](C4ISR_taccom_Eaton.pdf)
GC connectors                                                          | [579067.pdf](579067.pdf)
Glenair MIL-DTL-55116 Radio Connectors and Cables                      | [radio-connectors-and-cables.pdf](radio-connectors-and-cables.pdf)
Mil-Con, Inc. Military and NSA Connectors                              | [militaryandnsaconnectors.pdf](militaryandnsaconnectors.pdf)
Newark catalog number 133, page 1017                                   | [Newark_cat133_p1017.pdf](Newark_cat133_p1017.pdf)
Nexus Audio Frequency Plugs and Jacks                                  | [Nexus_Catalog-537765.pdf](Nexus_Catalog-537765.pdf)
WPI General Connector Interconnect Systems for Tactical Communications | [CI_GC_Catalog.pdf](CI_GC_Catalog.pdf)
